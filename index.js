function $(selector) {
    return document.querySelector(selector);
}

function $$(selector) {
    return document.querySelectorAll(selector);
}

console.log($('#alma'))
console.log($$('li'))

//Iterációk: NodeList
var lis = $$('li');

for (var i = 0; i < lis.length; i++) {
    var li = lis[i];
    console.log(li.innerHTML);
}

for (var li of lis) {
    console.log(li.innerHTML);
}

//Iterációk: Array
var lis = Array.from(lis);

for (var i = 0; i < lis.length; i++) {
    var li = lis[i];
    console.log(li.innerHTML);
}

for (var li of lis) {
    console.log(li.innerHTML);
}

lis.forEach(function (li) {
    console.log(li.innerHTML);
});
