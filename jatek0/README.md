# Játék 0

Első gondolatok az alkalmazás felépítéséről.

Három része lesz az alkalmazásnak:

- Az állapottér: úgy kell elképzelni, mint egy konzolos alkalmazást. Definiálja az állapotterét (ez egy sima JSON-szerű objektum), és kétféle függvényt. Az első féle függvények az állapottér változtatásáért felelősek (setterek, vagy redux terminológiával ezek az action-ök, csak itt függvényként jelennek meg), a másik féle függvények a kirajzolásnak biztosítják az adatokat (getterek, reduxban getState, csak itt részenként lekérdezhető). Az állapottér belső adataihoz csak úgy hozzáférni nem szabad. A privátságot sokféleképpen biztosíthatjuk, de itt az aláhúzással oldjuk meg. Az állapottér egységét fájl szinten biztosítjuk alapvetően, de be lehet foglalni az egészet egy struktúrába (objetktumba), sőt akár metódusok is rendelhetőek hozzá, de ezt nem erőltetném (ez már OOP), inkább a koncepció legyen meg, hogy leválasszuk az állapotteret az eseménykezelő és megjelenítő kódtól.
- A megjelenítő függvények: ezek végzik el az állapottér megjelenítését, HTML generálásáért (innerHTML) és DOM manipulálásért felelnek. Többféleképpen is megvalósítható: egy-egy objektum (vagy az egész alkalmazás) teljes újrarajzolását elvégezhetik, de lehet az oldal egy-egy részének kirajzolása a feladatuk. Ez az alkalmazástól függ. Hosszú távon ez az a réteg, amely virtuális DOM-mal dolgozik, és tényleg a teljes alkalmazás újrarajzolását rábízhatjuk átadván a teljes állapotteret (ld. react, react-redux).
- Az eseménykezelő függvények: a felhasználói felületről érkező események (felhasználói inputok) feldolgozására szolgál. Gyakorlatilag a vezérlő szerepét töltik be, mert ők mondják meg, hogy adott eseményre az állapottér hogyan változzon, és ezt hogyan kell kirajzolni. Ez egyfajta körforgás a vezérlő->modell->megjelenítés között. (Sokkal célszerűbb lenne, ha csak az állapotteret módosítanák, és maga az állapottérbeli változás kezdeményezné az újrarajzolást (ld react-redux). A reduxos világban egyébként ezek a dispatch+action együtt, hiszen egy inputra összegyűjti az adatokat és azokat beküldi a store-ba.) Mindig 3 részük van: beolvasás, feldolgozás, kiírás.

Ez a három egység három különböző fájlban helyezkedik el, így is jelezve a különböző funkciójukat:

- eseménykezelők (vezérlők)
- adatfeldolgozás (modell)
- megjelenítés (nézet)

Mivel mindegyik fájlban függvények vannak, ezért a sorrend majdnem mindegy. Először a seged.js-t kell betölteni. Ha kell, akkor vagy az eseménykezelők közé, vagy egy külön fájlban lehet az alkalmazás indítását elvégezni.

Kipróbálni, tesztelni külön is lehet őket. A modell részében a settereket hívogatjuk, és nézzük, hogy hogyan változik a state. A megjelenítésnél érdemes különválasztani azokat a függvényeket, amelyek elkérik az állapotot (draw), és azokat, amelyek valamilyen HTML generálnak (gen). Ezen utóbbiak tiszta függvények lévén megint csak önállóan tesztelhetők. Az eseménykezelőket nem kell tesztelni, talán az input oldalát lehetne, de azt elég console.log-gal.

