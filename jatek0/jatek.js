//Strict mode-ot script szinten állítsuk be!
"use strict";

//Eseménykezelők
//Vezérlők
$('#btnSize').addEventListener('click', onSetSize, false);
$('#tableContainer').addEventListener('click', onTableClick, false);

function onSetSize(e) {
  //Beolvasás
  var n = parseInt($('#size').value, 10);
  //Feldolgozás
  init(n);
  //Kiírás
  draw();
}

function onTableClick(e) {
  //Beolvasás
  if (e.target.tagName === 'TD') {
    var td = e.target;
    var tr = td.parentNode;
    var x = td.cellIndex;
    var y = tr.sectionRowIndex;
    //Feldolgozás
    increaseCell(x, y);
    //Kiírás
    // draw();
    drawCell(x, y);
  }
}

//Kirajzoló függvények
//A draw() rálát az állapottérre: ez addig igaz, amíg az állapotteret nem foglaljuk objektumba
//A draw() apró függvényekre bomlik, ezek már tiszta függvények, azaz mindent külső paraméterként kapnak

function draw() {
  $('#tableContainer').innerHTML = drawMap(getMap());
}

function drawCell(x, y) {
  $(`#tableContainer tr:nth-of-type(${y+1}) > td:nth-of-type(${x+1})`).innerHTML = getCell(x, y);
}

function drawMap(map) {
  //1. variáció: naív
  /*
  var s = '<table>';
  for (var i = 0; i < map.length; i++) {
    s += '<tr>';
    var row = map[i];
    for (var j = 0; j < row.length; j++) {
      var cell = row[j];
      s += '<td>' + cell + '</td>';
    }
    s += '</tr>';
  }
  s += '</table>';
  return s;
  */
  
  //2. variáció: join
  /*
  var rows = [];
  for (var i = 0; i < map.length; i++) {
    var row = map[i];
    rows.push('<td>' + row.join('</td><td>') + '</td>');
  }
  return '<table><tr>' + rows.join('</tr><tr>') + '</tr></table>'
  */
  
  //2b. variáció: join
  /*
  */
  var rows = [];
  for (var i = 0; i < map.length; i++) {
    var row = map[i];
    rows.push(`<td>${row.join('</td><td>')}</td>`);
  }
  return `<table><tr>${rows.join('</tr><tr>')}</tr></table>`;
  
  //3. variáció: reduce
  /*
  return '<table>' + 
    map.reduce(function (prev, curr) {
      return prev + '<tr>' + 
        curr.reduce(function (prev, curr) {
          return prev + '<td>' + curr + '</td>';
        }, '') + 
        '</tr>'; 
    }, '') +
    '</table>';
  */
  
  //3b. variáció: map + join
  /*
  return '<table>' + 
    map.map(function (row) {
      return '<tr>' + 
        row.map(function (cell) {
          return '<td>' + cell + '</td>';
        }).join('') + 
        '</tr>'; 
    }).join('') +
    '</table>';
  */
  
  //4. variáció: reduce fat arrow
  /*
  return '<table>' + 
    map.reduce((prev, curr) => prev + '<tr>' + 
        curr.reduce((prev, curr) => prev + '<td>' + curr + '</td>', '') + 
        '</tr>', '') +
    '</table>';
  */
     
  //5. variáció: template string
  /*
  return `
    <table>
      ${map.map(function (row) { return `
          <tr>
            ${row.map(function (cell) { return `
                <td>${cell}</td>`;
            }).join('')}
          </tr>`;
      }).join('')}
    </table>
  `;
  */
  
  
  //6. variáció: template string fat arrow
  /*
  return `
    <table>
      ${map.map(row => `
          <tr>
            ${row.map(cell => `
              <td>${cell}</td>`
            ).join('')}
          </tr>`
      ).join('')}
    </table>
  `;
  */
  
  
  //7. variáció: template string fat arrow reduce
  /*
  return `
    <table>
      ${map.reduce((prev, row) => prev + `
          <tr>
            ${row.reduce((prev, cell) => prev + `
              <td>${cell}</td>`
            , '')}
          </tr>`
      , '')}
    </table>
  `;
  */
}

console.log(drawMap(_clearMap(3)));

//Állapottér
var n;
var map;

function init(_n) {
  n = _n;
  map = _clearMap(n);
}

function increaseCell(x, y) {
  map[y][x] += 1;
  //Ki lehetne szervezni, de nem kell agyon szofisztikálni
  //_increaseCell(map, x, y): map
}

function getMap() {
  return map;
}

function getCell(x, y) {
  return map[y][x];
}

function _clearMap(n) {
  var map = [];
  for (var i = 0; i < n; i++) {
    map[i] = [];
    for (var j = 0; j < n; j++) {
      map[i].push(0);
    }
  }
  return map;
}

console.log(_clearMap(3));
