# Játék 1

- Az állapottér objektumba foglalása. Hosszú távon ez jobban megéri, mert rugalmasabb.
- A megjelenítés szétszedése.
- Az eseménykezelők mint vezérlők férnek hozzá egyedül az állapottérhez: ők settelik, és ők is gettelik azt. Így a megjelenítő függvények tiszta függvények maradhatnak, minden megjelenítendő dolgot paraméterként kapnak meg a draw függvények.
