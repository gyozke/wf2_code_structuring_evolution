//Strict mode-ot script szinten állítsuk be!
"use strict";

//Eseménykezelők
$('#btnSize').addEventListener('click', onSetSize, false);
$('#tableContainer').addEventListener('click', onTableClick, false);

function onSetSize(e) {
  //Beolvasás
  var n = parseInt($('#size').value, 10);
  //Feldolgozás
  init(n);
  //Kiírás
  drawMap(getMap());
}

function onTableClick(e) {
  //Beolvasás
  if (e.target.tagName === 'TD') {
    var td = e.target;
    var tr = td.parentNode;
    var x = td.cellIndex;
    var y = tr.sectionRowIndex;
    //Feldolgozás
    increaseCell(x, y);
    //Kiírás
    // draw();
    drawCell(x, y, getCell(x, y));
  }
}