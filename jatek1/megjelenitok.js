//Strict mode-ot script szinten állítsuk be!
"use strict";

//Kirajzoló függvények
//A draw() rálát az állapottérre: ez addig igaz, amíg az állapotteret nem foglaljuk objektumba
//A draw() apró függvényekre bomlik, ezek már tiszta függvények, azaz mindent külső paraméterként kapnak

function drawMap(map) {
  $('#tableContainer').innerHTML = _genTable(map);
}

function drawCell(x, y, value) {
  $(`#tableContainer tr:nth-of-type(${y+1}) > td:nth-of-type(${x+1})`).innerHTML = value;
}

function _genTable(map) {
  return `
  <table>
    ${map.map(function (row) {
      return _genRow(row);
    }).join('')}
  </table>`;
}

function _genRow(row) {
  return `
  <tr>
    ${row.map(function (cell) {
      return _genCell(cell);
    }).join('')}
  </tr>
  `;
}

function _genCell(cell) {
  return `<td>${cell}</td>`;
}

// console.log(genTable(_clearMap(3)));
