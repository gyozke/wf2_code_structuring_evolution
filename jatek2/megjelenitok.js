//Strict mode-ot script szinten állítsuk be!
"use strict";

//Kirajzoló függvények
//A draw() rálát az állapottérre: ez addig igaz, amíg az állapotteret nem foglaljuk objektumba
//A draw() apró függvényekre bomlik, ezek már tiszta függvények, azaz mindent külső paraméterként kapnak

var megjelenito = {};

megjelenito.drawMap = function drawMap() {
  $('#tableContainer').innerHTML = _genTable(adatkezelo.getMap());
}

megjelenito.drawCell = function drawCell(x, y) {
  $(`#tableContainer tr:nth-of-type(${y+1}) > td:nth-of-type(${x+1})`).innerHTML = adatkezelo.getCell(x, y);
}

function _genTable(map) {
  return `
  <table>
    ${map.map(function (row) {
      return _genRow(row);
    }).join('')}
  </table>`;
}

function _genRow(row) {
  return `
  <tr>
    ${row.map(function (cell) {
      return _genCell(cell);
    }).join('')}
  </tr>
  `;
}

function _genCell(cell) {
  return `<td>${cell}</td>`;
}

// console.log(genTable(_clearMap(3)));
