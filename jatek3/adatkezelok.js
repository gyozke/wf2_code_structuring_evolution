//Strict mode-ot script szinten állítsuk be!
"use strict";

//Állapottér

// var state = {
//   n: undefined,
//   map: undefined,
// };
//vagy
// var state = {};
//vagy
// var state = {
//   n: 0,
//   map: []
// };
//vagy
var _state = {
  n: 0,
  map: []
};
var adatkezelo = {};

//Setterek
adatkezelo.init = function init(n) {
  _state.n = n;
  _state.map = _clearMap(n);
}

adatkezelo.increaseCell = function increaseCell(x, y) {
  _state.map[y][x] += 1;
  //Ki lehetne szervezni, de nem kell agyon szofisztikálni
  //_increaseCell(map, x, y): map
}

//Getterek
adatkezelo.getMap = function getMap() {
  return _state.map;
}

adatkezelo.getCell = function getCell(x, y) {
  return _state.map[y][x];
}

//Belső függvények
function _clearMap(n) {
  var map = [];
  for (var i = 0; i < n; i++) {
    map[i] = [];
    for (var j = 0; j < n; j++) {
      map[i].push(0);
    }
  }
  return map;
}

// console.log(_clearMap(3));
