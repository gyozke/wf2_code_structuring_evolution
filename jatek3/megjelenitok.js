//Strict mode-ot script szinten állítsuk be!
"use strict";

//Kirajzoló függvények

var h = maquette.h;
var megjelenito = maquette.createProjector();
megjelenito.draw = megjelenito.scheduleRender;
// Initializes the projector 
document.addEventListener('DOMContentLoaded', function () {
  megjelenito.append($('#tableContainer'), _draw);
});

function _draw() {
  return _genTable(adatkezelo.getMap());
}

function _genTable(map) {
  return h('table', [
    map.map(function (row, i) {
        return _genRow(row, i);
    })
  ]);
}

function _genRow(row, i) {
  return h('tr', {key: i}, [
    row.map(function (cell, i) {
      return _genCell(cell, i);
    })
  ]);
}

function _genCell(cell, i) {
  return h('td', {key: i}, [cell]);
}

function _genTable2(map) {
  return h('table', [
    map.map(function (row, i) {
      return h('tr', {key: i}, [
        row.map(function (cell, i) {
          return h('td', {key: i}, [cell]);
        })
      ]);
    })
  ]);
}